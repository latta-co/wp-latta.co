<?php
	
/**
 * Template Name: Home
 * Author: Dave Latta
 * Author URI: https://www.latta.co
 */

get_header(); ?>

	<!-- Hero Section-->
	<div class="jumbotron jumbotron-fluid home-hero">
		<div class="container">
			<h1 class="display-3">Brand, Web, UI</h1>
			<p class="lead">Modern design and fullstack web development.</p>
			<button type="button" class="btn btn-primary">Get Started</button>
			<button type="button" class="btn btn-secondary">Connect</button>
		</div>
	</div>
	
	<!-- Trust Section-->
	<section class="trust-message">
		<div class="container">
			<!-- Row of columns -->
			<div class="row">
				<div class="col-md-12">
					<p>
	                 Empowering brands both big and small 
						<span style="font-weight: 400;">for over 10 years</span>
					</p>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Featured Projects-->
	<section id="featured-projects" class="section-gray ">
		<div class="container-fluid">
			<h1 class= "text-xs-center">Featured Work</h1>
			<p class="lead text-xs-center m-b-3">lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est</p>
			<div class="card-deck-wrapper">
			  <div class="card-deck">
			    <div class="card">
			      <img class="card-img-top img-fluid" src="https://unsplash.it/600/400" alt="Card image cap">
			      <div class="card-block">
			        <h5 class="card-title">Card title</h5>
			        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
			        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			      </div>
			    </div>
			    <div class="card">
			      <img class="card-img-top img-fluid" src="https://unsplash.it/600/400" alt="Card image cap">
			      <div class="card-block">
			        <h5 class="card-title">Card title</h5>
			        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
			        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			      </div>
			    </div>
			    <div class="card">
			      <img class="card-img-top img-fluid" src="https://unsplash.it/600/400" alt="Card image cap">
			      <div class="card-block">
			        <h5 class="card-title">Card title</h5>
			        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
			        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
			      </div>
			    </div>
			  </div>
			</div>
			
		</div>
	</section>

	
	<section class="section-gray">
		<div class="container">
			<!-- Row of columns -->
			<div class="row">
				<div class="col-sm-12">
					<div class="over-heading">
                        Why get a quote?
                    </div>
					<h1>It's fast, free and easy</h1>
				</div>
			</div>
			<!-- /row -->
			<!-- Row of columns -->
			<div class="row">
				<div class="col-sm-12">
					<img src="img/quote-range-screen.jpg" alt="">
					</div>
					<div class="col-sm-4">
						<h3 class="h5">There’s no obligation</h3>
						<p>Your quote comes with absolutely no strings attached. It takes 
							<span style="font-weight: 500;">less than two minutes</span> to see a range of quotes based on your unique needs and circumstances.
						</p>
					</div>
					<div class="col-sm-4">
						<h3 class="h5">Get expert guidance</h3>
						<p>You'll get FREE expert guidance. On average, our Life Insurance Professionals have 22 years of experience and are committed to ensuring you get the right policy at the right price.</p>
					</div>
					<div class="col-sm-4">
						<h3 class="h5">We’re here for you</h3>
						<p>If you have questions, our experienced Life Insurance Professionals will be happy to answer them for you.</p>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
			
		</section>
		<section class="why-term">
			<div class="container">
				<!-- Row of columns -->
				<div class="row">
					<div class="col-sm-12">
						<h1>Why choose term life?</h1>
					</div>
				</div>
				<!-- /row -->
				<div class="row">
					<div class="col-sm-6">
						<h3>It will give your family flexible protection</h3>
						<p>With 10- to 30-year terms, term life insurance provides tax-free money that your loved ones can use to meet many financial needs:</p>
						<ul class="fa-ul">
							<li>
								<i class="fa-li fa fa-check-circle"></i>Income replacement
							</li>
							<li>
								<i class="fa-li fa fa-check-circle"></i>Mortgage payments
							</li>
							<li>
								<i class="fa-li fa fa-check-circle"></i>Debt payments
							</li>
							<li>
								<i class="fa-li fa fa-check-circle"></i>Tuition or elder care expenses!
							</li>
						</ul>
						<br>
							<h3>It’s easy and affordable.</h3>
							<p>Term life may be far easier to set up and more affordable to maintain than you might imagine — especially when our Life Insurance Specialists are here to help you every step of the way.</p>
						</div>
						<div class="col-sm-6">
							<img src="img/Why-term-illustration.gif" alt="">
							</div>
						</div>
						<!-- /row -->
					</div>
					<!-- /container -->
				</section>
				<!-- Intro Section -->
				<section class="section-gray why-sbli">
					<div class="container">
						<!-- Row of columns -->
						<div class="row">
							<div class="col-md-9">
								<h1>Why SBLI?</h1>
								<p class="lead">SBLI was founded in 1907 by future Supreme Court Chief Justice Louis Brandeis to embody his belief that dependable
life insurance should be available to everyone who needs it, not just the wealthy. To this day, that philosophy drives
every decision we make, for every one of the over 1,000,000 families we’ve served over the last century.</p>
							</div>
						</div>
						<!-- Row of columns -->
						<div class="row ">
							<div class="col-md-6">
								<img src="img/shutterstock_298076636.jpg" alt="">
								</div>
								<div class="col-md-6">
									<img src="img/shutterstock_450844792-sm.jpg" alt="">
									</div>
								</div>
								<!-- Row of columns -->
								<div class="row ">
									<div class="col-md-6">
										<br>
											<h3>We find solutions that fit</h3>
											<p>Our seasoned Life Insurance Professionals will make sure you get a solution that fits your needs and budget.</p>
										</div>
										<div class="col-md-6">
											<br>
												<h3>We put your needs first</h3>
												<p>We don’t have shareholders. So we’re accountable to you, not a bunch of investors.</p>
											</div>
										</div>
										<div class="row ">
											<br>
												<div class="col-md-6">
													<h3>We don’t offer the kitchen sink</h3>
													<p>No auto insurance. No flood insurance. No credit cards. We just do one thing — life insurance. And we’d like to think this singular focus helps us do it better than anyone else.</p>
												</div>
												<div class="col-md-6">
													<h3>We’re not a corporate giant</h3>
													<p>We’re a small company. We treat our customers like people, not account numbers.</p>
												</div>
											</div>
										</div>
									</section> 


<?php
get_footer();
